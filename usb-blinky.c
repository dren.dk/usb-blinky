#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>

#include "light_ws2812.h"
#include "Descriptors.h"
#include "board.h"

#include <LUFA/Drivers/Board/LEDs.h>
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Platform/Platform.h>

/* Macros: */
/** LED mask for the library LED driver, to indicate that the USB interface is not ready. */
#define LEDMASK_USB_NOTREADY      LEDS_LED1

/** LED mask for the library LED driver, to indicate that the USB interface is enumerating. */
#define LEDMASK_USB_ENUMERATING  (LEDS_LED2 | LEDS_LED3)

/** LED mask for the library LED driver, to indicate that the USB interface is ready. */
#define LEDMASK_USB_READY        (LEDS_LED2 | LEDS_LED4)

/** LED mask for the library LED driver, to indicate that an error has occurred in the USB interface. */
#define LEDMASK_USB_ERROR        (LEDS_LED1 | LEDS_LED3)

USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface =
  {
   .Config =
   {
    .ControlInterfaceNumber   = INTERFACE_ID_CDC_CCI,
    .DataINEndpoint           =
    {
     .Address          = CDC_TX_EPADDR,
     .Size             = CDC_TXRX_EPSIZE,
     .Banks            = 1,
    },
    .DataOUTEndpoint =
    {
     .Address          = CDC_RX_EPADDR,
     .Size             = CDC_TXRX_EPSIZE,
     .Banks            = 1,
    },
    .NotificationEndpoint =
    {
     .Address          = CDC_NOTIFICATION_EPADDR,
     .Size             = CDC_NOTIFICATION_EPSIZE,
     .Banks            = 1,
    },
   },
  };


typedef struct {
  uint8_t ch;
  uint8_t stride;
  uint8_t offset;
} TestPattern;

#define TEST_PATTERNS 8
const TestPattern testPatterns[TEST_PATTERNS] =
  {
   // WS2812b (rgb)
   {'r', 3, 0},
   {'g', 3, 1},
   {'l', 3, 2},
   {'w', 1, 0},

   // SK6812 (rgb+w)
   {'G', 4, 0},
   {'R', 4, 1},
   {'L', 4, 2},
   {'W', 4, 3}
  };

/** Standard file stream for the CDC interface when set up, so that the virtual CDC COM port can be
 *  used like any regular character stream in the C APIs.
 */
static FILE USBSerialStream;

uint16_t pixelLen = 0;
uint8_t pixels[500*4];

void sendPixels(void) {
  ws2812_sendarray(pixels, sizeof(pixels));
  //fprintf(&USBSerialStream, "%i/%i\r\n",pixelLen, sizeof(pixels));
  pixelLen = 0;
  memset(pixels, 0, sizeof(pixels));
}

uint16_t buffy = 0;

void addBuffy(void) {
  if (pixelLen >= sizeof(pixels)) {
    pixelLen = 0;
    fprintf(&USBSerialStream, "Overflow, buffer reset, max subpixel count: %i\r\n", sizeof(pixels));    
  }
  
  pixels[pixelLen++] = buffy;
  buffy = 0;
}

void handleInputChar(uint8_t ch) {
 
  for (int i=0;i<TEST_PATTERNS;i++) {
    const TestPattern *tp = testPatterns+i;
    if (ch == tp->ch) {
      memset(pixels, 0, sizeof(pixels));
      
      for (int i=tp->offset ; i<sizeof(pixels) ; i += tp->stride) {
	pixels[i] = 0xff;
      }
      pixelLen = sizeof(pixels);

      sendPixels();
      return;
    }
  }

  if (ch == '=') {
    pixelLen = buffy;
    buffy=0;
    
  } else if (ch == '\n' || ch =='\r') {
    addBuffy();
    sendPixels();
    
  } else if (ch == ',') {
    addBuffy();
    
  } else if (ch >= '0' && ch <= '9') {
    buffy <<= 4;
    buffy += ch-'0';

  } else if (ch >= 'a' && ch <= 'f') {
    buffy <<= 4;
    buffy += ch-'a' + 10;

  } else if (ch >= 'A' && ch <= 'F') {
    buffy <<= 4;
    buffy += ch-'A' + 10;

  } else {
    buffy = 0;
    fprintf(&USBSerialStream, "Bad input, buffer cleared: '%c'\r\n", ch);
    fprintf(&USBSerialStream, "Syntax:\r\n");
    fprintf(&USBSerialStream, "  SK2812b: rr,gg,bb\r\n");
    fprintf(&USBSerialStream, "  SK6812:  gg,rr,bb,ww\r\n");
    fprintf(&USBSerialStream, "sub-pixels in hex 0-ff, comma separated\r\n");
    fprintf(&USBSerialStream, "Send full string with newline.\r\nMax sub-pixels is %i\r\n", sizeof(pixels));
  }  
}

void handleInput(void) {
  while (1) {
    int16_t ch = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
    if (ch < 0 || ch == ' ' || ch == '\t') {
      // No input, bail out.
      return;
    }
    handleInputChar(ch);
  }
}

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void SetupHardware(void)
{

  LEDs_Init();
  USB_Init();
  
  GPOUTPUT(LED_STRING);
}


/** Main program entry point. This routine contains the overall program flow, including initial
 *  setup of all components and the main program loop.
 */
int main(void)
{
  MCUSR &= ~(1 << WDRF);
  wdt_enable(WDTO_4S);
  clock_prescale_set(clock_div_1);  
  
  SetupHardware();

  /* Create a regular character stream for the interface so that it can be used with the stdio.h functions */
  CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);

  LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
  GlobalInterruptEnable();
  sendPixels();
  
  for (;;) {
    wdt_reset();

    handleInput();

    CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
    USB_USBTask();
  }
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
  LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
  LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
  bool ConfigSuccess = true;

  ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);

  LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
  CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}

/** CDC class driver callback function the processing of changes to the virtual
 *  control lines sent from the host..
 *
 *  \param[in] CDCInterfaceInfo  Pointer to the CDC class interface configuration structure being referenced
 */
void EVENT_CDC_Device_ControLineStateChanged(USB_ClassInfo_CDC_Device_t *const CDCInterfaceInfo)
{
  /* You can get changes to the virtual CDC lines in this callback; a common
     use-case is to use the Data Terminal Ready (DTR) flag to enable and
     disable CDC communications in your application when set to avoid the
     application blocking while waiting for a host to become ready and read
     in the pending data from the USB endpoints.
  */
  //bool HostReady = (CDCInterfaceInfo->State.ControlLineStates.HostToDevice & CDC_CONTROL_LINE_OUT_DTR) != 0;
}
