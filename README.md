= USB-Blinky: Control ws2812 from USB

The WS8212-style protocol is highly timing sensitive and is completely
non-standard, so the only thing that can really drive them is a GPIO pin
connected to a controller that's not doing anything else while updating
the pixels.

Modern computers are doing other things than bitbanging protocols and
they rarely have GPIO pins to spare.

To make it easy for modern computers to control ws2812 strings, I have made a
simple USB device that reads data from USB as though it was a serial port
and bangs out that data onto a GPIO pin.

Only one pin on the controller is connected to something non-obvious:

PE6 is connected to data in on the first WS2812B LED.

